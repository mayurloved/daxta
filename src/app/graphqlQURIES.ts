import { gql } from 'apollo-angular';

export const GET_POSTS_OF_AUTHOR = gql`
  query user($id: ID!) {
    user(id: $id) {
      posts {
        data {
          id
          title
        }
      }
    }
  }
`;

export const GET_ALL_POSTS = gql`
  {
    posts {
      data {
        id
        title
      }
    }
  }
`;

export const LOGIN = gql`
  mutation createComment($input: CreateCommentInput!) {
    createComment(input: $input) {
      email
      name
      body
    }
  }
`;
