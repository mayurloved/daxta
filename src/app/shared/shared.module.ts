
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HomeHeaderComponent } from './home-header/home-header.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [HomeHeaderComponent ],
  exports: [HomeHeaderComponent]
})
export class SharedModule {}