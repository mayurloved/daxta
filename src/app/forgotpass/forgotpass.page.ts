import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-forgotpass',
  templateUrl: './forgotpass.page.html',
  styleUrls: ['./forgotpass.page.scss'],
})
export class ForgotpassPage implements OnInit {

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(public alrtCtrl: AlertController, private router :Router) { }

  ngOnInit() {
  }

  async changePassword() {
    const alert = await this.alrtCtrl.create({
      message:
        '<img class="success-icon" src="../../../assets/images/_password.png" class="card-alert"/><div class="success-content">A link has been sent to your e-mail ID</div>',
    });
    await alert.present();
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  back(){
    this.router.navigateByUrl('/login')
  }
}
