import { Component, ViewChild } from '@angular/core';
import {
  AlertController,
  NavController,
  Platform,
  IonRouterOutlet,
} from '@ionic/angular';
import { AuthenticationService } from './services/authentication/authentication.service';
import { StorageService } from './services/storage/storage.service';
import { Location } from '@angular/common';
import { Plugins } from '@capacitor/core';
import {
  NetworkService,
  ConnectionStatus,
} from './services/network/network.service';
import { Router } from '@angular/router';
import { StatusBar, Style } from '@capacitor/status-bar';
const { App } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @ViewChild(IonRouterOutlet, { static: true }) routerOutlet: IonRouterOutlet;
  loginData: any;
  constructor(
    private platform: Platform,
    public authentication: AuthenticationService,
    public navCtrl: NavController,
    public storageservice: StorageService,
    public alrtCtrl: AlertController,
    private location: Location,
    private networkService: NetworkService,
    public router: Router
  ) {
    this.initializeApp();
    this.backButtonEvent();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.setStyle({ style: Style.Default });
      // this.authenticationService.authState.subscribe(state => {
      this.storageservice.get('userdata').then((res) => {
        if (res) {
          this.navCtrl.navigateRoot('/menu');
        } else {
          this.navCtrl.navigateRoot('/login');
        }
      });
    });
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(10, () => {
      this.storageservice.get('userdata').then((res) => {
        if (
          (res && this.router.url == '/menu/first') ||
          ((res == '' || res == null || res == undefined) &&
            this.router.url == '/login')
        ) {
          this.backButtonAlert();
        } else {
          this.location.back();
        }
      });
      // if (!this.routerOutlet.canGoBack()) {
      //   this.backButtonAlert();
      // } else {
      //   this.location.back();
      // }
    });
  }

  async backButtonAlert() {
    const alert = await this.alrtCtrl.create({
      message: 'Sure, Want to Exit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            navigator['app'].exitApp();
          },
        },
      ],
    });
    await alert.present();
  }
}
