import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  authState = new BehaviorSubject(false);

  constructor(
    public storageservice: StorageService,
    public platform: Platform
  ) {
    //  this.platform.ready().then(() => {
    this.ifLoggedIn();
    // });
  }

  ifLoggedIn() {
    this.storageservice.get('userdata');
    this.authState.next(true);
  }

  login(user) {
    this.storageservice.set('userdata', user);
    this.authState.next(true);
  }

  logout() {
    this.storageservice.remove('userdata');
    this.authState.next(false);
  }

  isAuthenticated() {
    return this.authState.value;
  }
}
