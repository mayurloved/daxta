import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddNotesComponent } from './add-notes/add-notes.component';
import { FirstPage } from './first.page';
import { SessionScreenComponent } from './session-screen/session-screen.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {
    path: '',
    component: FirstPage,
  },
  {
    path: 'session-screen',
    component: SessionScreenComponent,
  },
  {
    path: 'summary',
    component: SummaryComponent,
  },
  {
    path: 'addnotes',
    component: AddNotesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstPageRoutingModule {}
