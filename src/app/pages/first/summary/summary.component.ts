import { StorageService } from './../../../services/storage/storage.service';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, Platform, AlertController, NavController } from '@ionic/angular';
// import { MyModalPage } from '../../modals/my-modal/my-modal.page';
import { LocationPage } from '../../modals/location/location.page';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
  dataReturned: any;
  buttonClicked: boolean = false;
  isShowDiv = false;
  constructor(private router : Router , private platform: Platform,
    public authentication: AuthenticationService,
    public alrtCtrl: AlertController,
    public storageservice: StorageService,
    public navCtrl: NavController,
    public modalController: ModalController) { }

  ngOnInit() {}
  async openModal() {
    const modal = await this.modalController.create({
      component: LocationPage,
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
    return await modal.present();
  }

  async logOut() {
    const alert = await this.alrtCtrl.create({
      message: 
      '<img class="success-icon" src="../../../../assets/icon/warning.png" /><div class="success-content">End session?</div>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            this.authentication.logout();
            this.storageservice.clear();
            this.navCtrl.navigateRoot(['/first']);
          },
        },
      ],
    });
    await alert.present();
  }


  closenote(){
    this.router.navigateByUrl('/menu/first/session-screen')
  }
  onButtonClick() {
    this.buttonClicked = !this.buttonClicked;
}
}

