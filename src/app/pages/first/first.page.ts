import { MyModalPage } from './../modals/my-modal/my-modal.page';
import { StorageService } from './../../services/storage/storage.service';
import { Component, OnInit } from '@angular/core';
import { Platform, NavController, ModalController } from '@ionic/angular';
import {
  ConnectionStatus,
  NetworkService,
} from 'src/app/services/network/network.service';
import { Apollo } from 'apollo-angular';
import { GET_ALL_POSTS } from 'src/app/graphqlQURIES';

@Component({
  selector: 'app-first',
  templateUrl: './first.page.html',
  styleUrls: ['./first.page.scss'],
})
export class FirstPage implements OnInit {
  isShown = true;
  posts: any = [];
  buttonActive: boolean = true;
  view: any;
  dataReturned: any;
  constructor(
    private platform: Platform,
    public navCtrl: NavController,

    private apollo: Apollo,
    private networkService: NetworkService,
    public storageservice: StorageService,
    public modalController: ModalController
  ) {
    this.view = 'meetings';

    this.networkService
      .onNetworkChange()
      .subscribe((status: ConnectionStatus) => {
        if (status === ConnectionStatus.Offline) {
          // alert('user is offline now, store current data');
        } else if (status === ConnectionStatus.Online) {
          // alert('user is online now, get data');
        }
      });
  }

  ngOnInit(): void {
    this.getAllPosts();
    this.accordionToggle();
  }
  
changeView(view) {
  this.view = view;
}

  getAllPosts() {
    this.apollo
      .watchQuery({
        query: GET_ALL_POSTS,
      })
      .valueChanges.subscribe((result: any) => {
        this.posts = result?.data?.posts?.data;
      });
  }

  showSideDrawer() {
    this.isShown = !this.isShown;
  }

  startSession(){
    this.navCtrl.navigateForward('/menu/first/session-screen');
  }

  accordionToggle() {
    console.log("workds")
    var acc = document.getElementsByClassName('accordion');
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function () {
        this.classList.toggle('active');
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + 'px';
        }
      });
    }
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: MyModalPage,
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
    return await modal.present();
  }

  
  // ionViewWillLeave() {
  //   this.platform.backButton.subscribeWithPriority(15, () => {
  //     alert('Second page Handler was called!');
  //     this.navCtrl.back();
  //   });
  // }
}
