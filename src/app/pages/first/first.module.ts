import { SummaryComponent } from './summary/summary.component';
import { AddNotesComponent } from './add-notes/add-notes.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FirstPageRoutingModule } from './first-routing.module';
import { FirstPage } from './first.page';
import { SessionScreenComponent } from './session-screen/session-screen.component';
import { IonicModule } from '@ionic/angular';
import { MyModalPageModule } from '../modals/my-modal/my-modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeHeaderComponent } from 'src/app/shared/home-header/home-header.component';

@NgModule({
  imports: [CommonModule, FormsModule, FirstPageRoutingModule, IonicModule, MyModalPageModule, SharedModule],
  declarations: [FirstPage, SessionScreenComponent, AddNotesComponent, SummaryComponent]
})
export class FirstPageModule {}
