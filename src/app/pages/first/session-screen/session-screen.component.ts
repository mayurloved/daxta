import { Component, OnInit } from '@angular/core';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ModalController } from '@ionic/angular';
import { MyModalPage } from '../../modals/my-modal/my-modal.page';

@Component({
  selector: 'app-session-screen',
  templateUrl: './session-screen.component.html',
  styleUrls: ['./session-screen.component.scss'],
})
export class SessionScreenComponent implements OnInit {
   currentNumber = 0;
  dataReturned: any;
  showId = true;
  constructor(
    private platform: Platform,
    public authentication: AuthenticationService,
    public alrtCtrl: AlertController,
    public storageservice: StorageService,
    public navCtrl: NavController,
    public modalController: ModalController
  ) {}

  ngOnInit() {
    this.collapseToggle();
  }

   increment () {
    this.currentNumber++;
  }
  
   decrement () {
    this.currentNumber--;
  }

  
  onfocus(){
    function focusMe(button) {
      var elem = document.getElementsByClassName("button-selected")[0];      
      // if element having class `"button-selected"` defined, do stuff
      if (elem) {
        elem.className = "";
      }
      button.className = "button-selected";
    }
  }

  async logOut() {
    const alert = await this.alrtCtrl.create({
      message: 
      '<img class="success-icon" src="../../../../assets/icon/warning.png" /><div class="success-content">End session?</div>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            this.authentication.logout();
            this.storageservice.clear();
            this.navCtrl.navigateRoot(['/first']);
          },
        },
      ],
    });
    await alert.present();
  }


  
  async recorddata() {
    const alert = await this.alrtCtrl.create({
      message: 
      '<img class="success-icon" src="../../../../assets/icon/warning.png" /><div class="success-content">Please fill something to record the data</div>',
      // buttons: [
      //   {
      //     text: 'Cancel',
      //     role: 'cancel',
      //   },
      //   {
      //     text: 'Yes',
      //     handler: () => {
      //       this.authentication.logout();
      //       this.storageservice.clear();
      //       this.navCtrl.navigateRoot(['/login']);
      //     },
      //   },
      // ],
    });
    await alert.present();
  }

  toggleId() {
    this.showId = !this.showId;
  }

  collapseToggle() {
   
    var acc = document.getElementsByClassName('collapse-div');
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function () {
        this.classList.toggle('active');
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + 'px';
        }
      });
    }
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: MyModalPage,
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
    return await modal.present();
  }

}
