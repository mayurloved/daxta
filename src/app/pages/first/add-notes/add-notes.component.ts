import { StorageService } from './../../../services/storage/storage.service';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { Platform, AlertController, NavController, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-notes',
  templateUrl: './add-notes.component.html',
  styleUrls: ['./add-notes.component.scss'],
})
export class AddNotesComponent implements OnInit {

  constructor(private router : Router ,
    private platform: Platform,
    public authentication: AuthenticationService,
    public alrtCtrl: AlertController,
    public storageservice: StorageService,
    public navCtrl: NavController,
    public modalController: ModalController) { }

  ngOnInit() {}
  savenote(){
    this.router.navigateByUrl('/menu/first/session-screen')
  }
  async logOut() {
    const alert = await this.alrtCtrl.create({
      message: 
      '<img class="success-icon" src="../../../../assets/icon/warning.png" /><div class="success-content">End session??</div>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            this.authentication.logout();
            this.storageservice.clear();
            this.navCtrl.navigateRoot(['/first']);
          },
        },
      ],
    });
    await alert.present();
  }
}
