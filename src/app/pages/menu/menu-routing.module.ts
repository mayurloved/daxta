import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/services/authguard/auth-guard.service';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'menu',
    component: MenuPage,
    children: [
      {
        path: '',
        redirectTo: '/menu/first',
        pathMatch: 'full',
      },
      // {
      //   path: 'home',
      //   loadChildren: () =>
      //     import('../home/home.module').then((m) => m.HomeModule),
      // },
      {
        path: 'first',
        loadChildren: () =>
          import('../first/first.module').then((m) => m.FirstPageModule),
      },
      {
        path: 'second',
        loadChildren: () =>
          import('../second/second.module').then((m) => m.SecondPageModule),
      },
      {
        path: 'third',
        loadChildren: () =>
          import('../third/third.module').then((m) => m.ThirdPageModule),
      },
    ],
    canActivate: [AuthGuardService],
  },
  {
    path: '',
    redirectTo: '/menu/first',
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
