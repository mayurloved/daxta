import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { Platform, NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  pages = [
    {
      title: '../../../assets/images/list.png',
      url: '/menu/first',
    },
    // {
    //   title: '../../../assets/images/password.png',
    //   url: '/menu/second',
    // },
    {
      title: '../../../assets/images/password_icon.png',
      url: '/menu/third',
    },
  ];

  selectedPath = '';

  constructor(
    private router: Router,
    private platform: Platform,
    public authentication: AuthenticationService,
    public alrtCtrl: AlertController,
    public storageservice: StorageService,
    public navCtrl: NavController
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
      // console.log(this.selectedPath);
      if (
        this.selectedPath == undefined ||
        this.selectedPath == null ||
        this.selectedPath == '' ||
        this.selectedPath == '/menu' ||
        this.selectedPath == '/menu/first/session-screen'
      ) {
        this.selectedPath = '/menu/first';
      } else {
        this.selectedPath = event.url;
      }
    });
  }

  ngOnInit() {}

  async logOut() {
    const alert = await this.alrtCtrl.create({
      message:
        '<img class="warning-icon" src="../../../assets/icon/right.png" class="card-alert"/><div class="exitapp-content">Are you sure to logout ?</div>',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.authentication.logout();
            this.storageservice.clear();
            this.navCtrl.navigateRoot(['/login']);
          },
          cssClass: 'alert-ok-button',
        },
        {
          text: 'No',
          role: 'no',
          cssClass: 'alert-cancel-button',
        }
      ],
    });
    await alert.present();
  }

  gotoHome() {
    this.navCtrl.navigateRoot('/menu');
  }
}
