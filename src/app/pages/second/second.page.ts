import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, NavController, Platform } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage/storage.service';
const { App } = Plugins;
import { Apollo } from 'apollo-angular';
import { GET_POSTS_OF_AUTHOR } from 'src/app/graphqlQURIES';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-second',
  templateUrl: './second.page.html',
  styleUrls: ['./second.page.scss'],
})
export class SecondPage implements OnInit {
  posts: any = [];
  constructor(
    private platform: Platform,
    private routerOutlet: IonRouterOutlet,
    public navCtrl: NavController,
    public router: Router,
    public storageservice: StorageService,
    private apollo: Apollo,
    public alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.getPostofAuthor();
    this.accordionToggle();
  }

  accordionToggle() {
    var acc = document.getElementsByClassName('accordion');
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function () {
        this.classList.toggle('active');
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + 'px';
        }
      });
    }
  }

  getPostofAuthor() {
    this.apollo
      .watchQuery({
        query: GET_POSTS_OF_AUTHOR,
        variables: {
          id: 1,
        },
      })
      .valueChanges.subscribe((result: any) => {
        this.posts = result?.data?.user?.posts?.data;
        this.storageservice.set('myValue', this.posts);
      });
  }

  async showAlert(){
    const alert = await this.alertController.create({    
      header: 'The information you have provided is incomplete or invalid. Please check your entries and check again.'             
    });
    alert.present();
    setTimeout(()=>{
        alert.dismiss();
    }, 2000);
  }
}
