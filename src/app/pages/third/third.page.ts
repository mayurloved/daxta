import { Component, OnInit } from '@angular/core';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-third',
  templateUrl: './third.page.html',
  styleUrls: ['./third.page.scss'],
})
export class ThirdPage implements OnInit {
  showId = true;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  constructor(
    private platform: Platform,
    public authentication: AuthenticationService,
    public alrtCtrl: AlertController,
    public storageservice: StorageService,
    public navCtrl: NavController
  ) {}

  ngOnInit() {}

  async changePassword() {
    const alert = await this.alrtCtrl.create({
      message:
        '<img class="success-icon" src="../../../assets/images/accept.png" class="card-alert"/><div class="success-content">Password changed successfully!</div>',
    });
    await alert.present();
  }

  async Emaillink() {
    const alert = await this.alrtCtrl.create({
      message:
        '<img class="success-icon"   src="../../../assets/icon/Group 873.png" class="card-alert"/><div class="success-content">A link has been sent to your e-mail ID</div>',
    });
    await alert.present();
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  gotoForgotPass(){
    console.log("www")
    this.navCtrl.navigateForward('/forgotpass');
  }
}
