import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ThirdPageRoutingModule } from './third-routing.module';

import { ThirdPage } from './third.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ThirdPageRoutingModule,
    SharedModule
  ],
  declarations: [ThirdPage]
})
export class ThirdPageModule {}
