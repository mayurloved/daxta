import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IonRouterOutlet, NavController, Platform } from '@ionic/angular';
import { Apollo } from 'apollo-angular';
import { LOGIN } from '../graphqlQURIES';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { Plugins } from '@capacitor/core';
import { MainService } from '../services/main/main.service';
const { App } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  loginFormSubmited: any = false;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(
    public mainservice: MainService,
    private routerOutlet: IonRouterOutlet,
    public platform: Platform,
    public formBuilder: FormBuilder,
    private apollo: Apollo,
    public auth: AuthenticationService,
    public navCtrl: NavController
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });
  }

  login() {
    this.mainservice.present();
    this.loginFormSubmited = true;
    let formValues = this.loginForm.value;
    this.apollo
      .mutate({
        mutation: LOGIN,
        variables: {
          input: {
            email: formValues.email,
            name: formValues.password,
            body: 'ssdds',
          },
        },
      })
      .subscribe(
        ({ data }) => {
          this.auth.login(data);
          // debugger;
          this.navCtrl.navigateRoot(['/menu']);
          this.mainservice.dismiss();
        },
        (error) => {
          this.mainservice.dismiss();
          console.log('there was an error sending the query', error);
        }
      );
  }

  forgotpass(){
    console.log("ddd");
    this.navCtrl.navigateForward('forgotpass')
    // this.navCtrl.navigateRoot(['/forgot-pass']);
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
}
