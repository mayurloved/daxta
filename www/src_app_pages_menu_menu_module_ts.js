(self["webpackChunkDaxta"] = self["webpackChunkDaxta"] || []).push([["src_app_pages_menu_menu_module_ts"],{

/***/ 4908:
/*!*****************************************************!*\
  !*** ./src/app/pages/first/first-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirstPageRoutingModule": () => (/* binding */ FirstPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _first_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./first.page */ 1766);
/* harmony import */ var _session_screen_session_screen_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./session-screen/session-screen.component */ 3688);





const routes = [
    {
        path: '',
        component: _first_page__WEBPACK_IMPORTED_MODULE_0__.FirstPage,
    },
    {
        path: 'session-screen',
        component: _session_screen_session_screen_component__WEBPACK_IMPORTED_MODULE_1__.SessionScreenComponent,
    },
];
let FirstPageRoutingModule = class FirstPageRoutingModule {
};
FirstPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule],
    })
], FirstPageRoutingModule);



/***/ }),

/***/ 7768:
/*!*********************************************!*\
  !*** ./src/app/pages/first/first.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirstPageModule": () => (/* binding */ FirstPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _first_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./first-routing.module */ 4908);
/* harmony import */ var _first_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./first.page */ 1766);
/* harmony import */ var _session_screen_session_screen_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./session-screen/session-screen.component */ 3688);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);








let FirstPageModule = class FirstPageModule {
};
FirstPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule, _first_routing_module__WEBPACK_IMPORTED_MODULE_0__.FirstPageRoutingModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule],
        declarations: [_first_page__WEBPACK_IMPORTED_MODULE_1__.FirstPage, _session_screen_session_screen_component__WEBPACK_IMPORTED_MODULE_2__.SessionScreenComponent]
    })
], FirstPageModule);



/***/ }),

/***/ 1766:
/*!*******************************************!*\
  !*** ./src/app/pages/first/first.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirstPage": () => (/* binding */ FirstPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_first_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./first.page.html */ 7199);
/* harmony import */ var _first_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./first.page.scss */ 5180);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_services_network_network_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/network/network.service */ 1947);
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! apollo-angular */ 550);
/* harmony import */ var src_app_graphqlQURIES__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/graphqlQURIES */ 7436);








let FirstPage = class FirstPage {
    constructor(platform, navCtrl, apollo, networkService) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.apollo = apollo;
        this.networkService = networkService;
        this.isShown = true;
        this.posts = [];
        this.networkService
            .onNetworkChange()
            .subscribe((status) => {
            if (status === src_app_services_network_network_service__WEBPACK_IMPORTED_MODULE_2__.ConnectionStatus.Offline) {
                // alert('user is offline now, store current data');
            }
            else if (status === src_app_services_network_network_service__WEBPACK_IMPORTED_MODULE_2__.ConnectionStatus.Online) {
                // alert('user is online now, get data');
            }
        });
    }
    ngOnInit() {
        this.getAllPosts();
        this.accordionToggle();
    }
    getAllPosts() {
        this.apollo
            .watchQuery({
            query: src_app_graphqlQURIES__WEBPACK_IMPORTED_MODULE_3__.GET_ALL_POSTS,
        })
            .valueChanges.subscribe((result) => {
            var _a, _b;
            this.posts = (_b = (_a = result === null || result === void 0 ? void 0 : result.data) === null || _a === void 0 ? void 0 : _a.posts) === null || _b === void 0 ? void 0 : _b.data;
        });
    }
    showSideDrawer() {
        this.isShown = !this.isShown;
    }
    startSession() {
        this.navCtrl.navigateForward('/menu/first/session-screen');
    }
    accordionToggle() {
        console.log("workds");
        var acc = document.getElementsByClassName('accordion');
        var i;
        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener('click', function () {
                this.classList.toggle('active');
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                }
                else {
                    panel.style.maxHeight = panel.scrollHeight + 'px';
                }
            });
        }
    }
};
FirstPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.NavController },
    { type: apollo_angular__WEBPACK_IMPORTED_MODULE_5__.Apollo },
    { type: src_app_services_network_network_service__WEBPACK_IMPORTED_MODULE_2__.NetworkService }
];
FirstPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-first',
        template: _raw_loader_first_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_first_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FirstPage);



/***/ }),

/***/ 3688:
/*!************************************************************************!*\
  !*** ./src/app/pages/first/session-screen/session-screen.component.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SessionScreenComponent": () => (/* binding */ SessionScreenComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_session_screen_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./session-screen.component.html */ 6690);
/* harmony import */ var _session_screen_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./session-screen.component.scss */ 4385);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authentication/authentication.service */ 7020);
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/storage/storage.service */ 6578);







let SessionScreenComponent = class SessionScreenComponent {
    constructor(platform, authentication, alrtCtrl, storageservice, navCtrl) {
        this.platform = platform;
        this.authentication = authentication;
        this.alrtCtrl = alrtCtrl;
        this.storageservice = storageservice;
        this.navCtrl = navCtrl;
        this.showId = true;
    }
    ngOnInit() {
        this.collapseToggle();
    }
    // async logOut() {
    //   const alert = await this.alrtCtrl.create({
    //     message: 'Are you sure you want to logout ?',
    //     buttons: [
    //       {
    //         text: 'Cancel',
    //         role: 'cancel',
    //       },
    //       {
    //         text: 'Yes',
    //         handler: () => {
    //           this.authentication.logout();
    //           this.storageservice.clear();
    //           this.navCtrl.navigateRoot(['/login']);
    //         },
    //       },
    //     ],
    //   });
    //   await alert.present();
    // }
    toggleId() {
        this.showId = !this.showId;
    }
    collapseToggle() {
        var acc = document.getElementsByClassName('collapse-div');
        var i;
        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener('click', function () {
                this.classList.toggle('active');
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                }
                else {
                    panel.style.maxHeight = panel.scrollHeight + 'px';
                }
            });
        }
    }
};
SessionScreenComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.Platform },
    { type: src_app_services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__.AuthenticationService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.AlertController },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.NavController }
];
SessionScreenComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-session-screen',
        template: _raw_loader_session_screen_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_session_screen_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SessionScreenComponent);



/***/ }),

/***/ 8663:
/*!***************************************************!*\
  !*** ./src/app/pages/menu/menu-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MenuPageRoutingModule": () => (/* binding */ MenuPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var src_app_services_authguard_auth_guard_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/services/authguard/auth-guard.service */ 7023);
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./menu.page */ 7506);





const routes = [
    {
        path: 'menu',
        component: _menu_page__WEBPACK_IMPORTED_MODULE_1__.MenuPage,
        children: [
            {
                path: '',
                redirectTo: '/menu/first',
                pathMatch: 'full',
            },
            // {
            //   path: 'home',
            //   loadChildren: () =>
            //     import('../home/home.module').then((m) => m.HomeModule),
            // },
            {
                path: 'first',
                loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ../first/first.module */ 7768)).then((m) => m.FirstPageModule),
            },
            {
                path: 'second',
                loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_second_second_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../second/second.module */ 4702)).then((m) => m.SecondPageModule),
            },
            {
                path: 'third',
                loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_third_third_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../third/third.module */ 4812)).then((m) => m.ThirdPageModule),
            },
        ],
        canActivate: [src_app_services_authguard_auth_guard_service__WEBPACK_IMPORTED_MODULE_0__.AuthGuardService],
    },
    {
        path: '',
        redirectTo: '/menu/first',
        canActivate: [src_app_services_authguard_auth_guard_service__WEBPACK_IMPORTED_MODULE_0__.AuthGuardService],
    },
];
let MenuPageRoutingModule = class MenuPageRoutingModule {
};
MenuPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule],
    })
], MenuPageRoutingModule);



/***/ }),

/***/ 252:
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MenuPageModule": () => (/* binding */ MenuPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _menu_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./menu-routing.module */ 8663);
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./menu.page */ 7506);
/* harmony import */ var _first_first_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../first/first.module */ 7768);








let MenuPageModule = class MenuPageModule {
};
MenuPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule, _menu_routing_module__WEBPACK_IMPORTED_MODULE_0__.MenuPageRoutingModule, _first_first_module__WEBPACK_IMPORTED_MODULE_2__.FirstPageModule],
        declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_1__.MenuPage],
    })
], MenuPageModule);



/***/ }),

/***/ 7506:
/*!*****************************************!*\
  !*** ./src/app/pages/menu/menu.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MenuPage": () => (/* binding */ MenuPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_menu_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./menu.page.html */ 538);
/* harmony import */ var _menu_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./menu.page.scss */ 6954);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var src_app_services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authentication/authentication.service */ 7020);
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/storage/storage.service */ 6578);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);








let MenuPage = class MenuPage {
    constructor(router, platform, authentication, alrtCtrl, storageservice, navCtrl) {
        this.router = router;
        this.platform = platform;
        this.authentication = authentication;
        this.alrtCtrl = alrtCtrl;
        this.storageservice = storageservice;
        this.navCtrl = navCtrl;
        this.pages = [
            {
                title: '../../../assets/images/list.png',
                url: '/menu/first',
            },
            // {
            //   title: '../../../assets/images/password.png',
            //   url: '/menu/second',
            // },
            {
                title: '../../../assets/images/password.png',
                url: '/menu/third',
            },
        ];
        this.selectedPath = '';
        this.router.events.subscribe((event) => {
            this.selectedPath = event.url;
            // console.log(this.selectedPath);
            if (this.selectedPath == undefined ||
                this.selectedPath == null ||
                this.selectedPath == '' ||
                this.selectedPath == '/menu' ||
                this.selectedPath == '/menu/first/session-screen') {
                this.selectedPath = '/menu/first';
            }
            else {
                this.selectedPath = event.url;
            }
        });
    }
    ngOnInit() { }
    logOut() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alrtCtrl.create({
                message: '<img class="warning-icon" src="../../../assets/images/warning.svg" class="card-alert"/><div class="exitapp-content">Are you sure you want to logout ?</div>',
                buttons: [
                    {
                        text: 'No',
                        role: 'no',
                        cssClass: 'alert-cancel-button',
                    },
                    {
                        text: 'Yes',
                        handler: () => {
                            this.authentication.logout();
                            this.storageservice.clear();
                            this.navCtrl.navigateRoot(['/login']);
                        },
                        cssClass: 'alert-ok-button',
                    },
                ],
            });
            yield alert.present();
        });
    }
    gotoHome() {
        this.navCtrl.navigateRoot('/menu');
    }
};
MenuPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform },
    { type: src_app_services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__.AuthenticationService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.AlertController },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController }
];
MenuPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-menu',
        template: _raw_loader_menu_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_menu_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], MenuPage);



/***/ }),

/***/ 7023:
/*!**********************************************************!*\
  !*** ./src/app/services/authguard/auth-guard.service.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthGuardService": () => (/* binding */ AuthGuardService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../authentication/authentication.service */ 7020);



let AuthGuardService = class AuthGuardService {
    constructor(authentication) {
        this.authentication = authentication;
    }
    canActivate() {
        console.log("working");
        return this.authentication.isAuthenticated();
    }
};
AuthGuardService.ctorParameters = () => [
    { type: _authentication_authentication_service__WEBPACK_IMPORTED_MODULE_0__.AuthenticationService }
];
AuthGuardService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], AuthGuardService);



/***/ }),

/***/ 5180:
/*!*********************************************!*\
  !*** ./src/app/pages/first/first.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-grid {\n  padding: 0px !important;\n}\n\n.container-div {\n  display: flex;\n  flex-wrap: nowrap;\n}\n\n.div1Class {\n  width: 100%;\n}\n\n.div1Class2 {\n  width: 100%;\n}\n\n.div2Class {\n  min-height: calc(100vh - 220px);\n  width: 0px;\n  background-color: #faf6fd;\n  transition: width 0.3s ease;\n}\n\n.div2Class2 {\n  min-height: calc(100vh - 220px);\n  width: 400px;\n  background-color: #faf6fd;\n  transition: width 0.3s ease;\n}\n\n.hidepadding {\n  padding-left: 50px;\n  padding-right: 50px;\n}\n\n.showpadding {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n\nbutton {\n  width: 112px;\n  height: 33px;\n  margin-left: 10px;\n  margin-right: 10px;\n  background: var(--ion-color-primary);\n  color: #fff;\n}\n\n.accordion-col {\n  background-color: var(--ion-primary-background);\n  min-height: calc(100vh - 232px);\n  margin-left: 20px;\n  margin-right: 20px;\n}\n\n.accordion-col .accordion {\n  background-color: #eeebf6;\n  color: #444;\n  cursor: pointer;\n  padding: 12px;\n  width: 100%;\n  border: none;\n  text-align: left;\n  outline: none;\n  transition: 0.6s;\n  position: relative;\n}\n\n.accordion-col .active,\n.accordion-col .accordion:hover {\n  background-color: #eeebf6;\n}\n\n.accordion-col .accordion:after {\n  position: absolute;\n  right: 20px;\n  top: 5px;\n  transform: translate(50%, 50%);\n  content: url('Icon awesome-angle-down.png');\n}\n\n.accordion-col .active:after {\n  position: absolute;\n  right: 20px;\n  top: 5px;\n  transform: translate(50%, 50%);\n  content: url('Icon awesome-angle-up.png');\n}\n\n.accordion-col .panel {\n  background-color: #eeebf6;\n  max-height: 0;\n  overflow: hidden;\n  transition: max-height 0.4s ease-out;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpcnN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsaUJBQUE7QUFFSjs7QUFDQTtFQUNJLFdBQUE7QUFFSjs7QUFDQTtFQUNJLFdBQUE7QUFFSjs7QUFDQTtFQUNJLCtCQUFBO0VBQ0EsVUFBQTtFQUNBLHlCQUFBO0VBSUEsMkJBQUE7QUFFSjs7QUFDQTtFQUNJLCtCQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBSUEsMkJBQUE7QUFFSjs7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsbUJBQUE7QUFFSjs7QUFDQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUFFSjs7QUFDQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLG9DQUFBO0VBQ0EsV0FBQTtBQUVKOztBQUNBO0VBQ0ksK0NBQUE7RUFDQSwrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFFSjs7QUFESTtFQUNJLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBR1I7O0FBREk7O0VBRUkseUJBQUE7QUFHUjs7QUFESTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSw4QkFBQTtFQUNBLDJDQUFBO0FBR1I7O0FBREk7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsOEJBQUE7RUFDQSx5Q0FBQTtBQUdSOztBQURJO0VBQ0kseUJBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtBQUdSIiwiZmlsZSI6ImZpcnN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1ncmlkIHtcclxuICAgIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xyXG59XHJcbi5jb250YWluZXItZGl2IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IG5vd3JhcDtcclxufVxyXG5cclxuLmRpdjFDbGFzcyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmRpdjFDbGFzczIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5kaXYyQ2xhc3Mge1xyXG4gICAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDIyMHB4KTtcclxuICAgIHdpZHRoOiAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmFmNmZkO1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiB3aWR0aCAwLjNzIGVhc2U7XHJcbiAgICAtbW96LXRyYW5zaXRpb246IHdpZHRoIDAuM3MgZWFzZTtcclxuICAgIC1vLXRyYW5zaXRpb246IHdpZHRoIDAuM3MgZWFzZTtcclxuICAgIHRyYW5zaXRpb246IHdpZHRoIDAuM3MgZWFzZTtcclxufVxyXG5cclxuLmRpdjJDbGFzczIge1xyXG4gICAgbWluLWhlaWdodDogY2FsYygxMDB2aCAtIDIyMHB4KTtcclxuICAgIHdpZHRoOiA0MDBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmYWY2ZmQ7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IHdpZHRoIDAuM3MgZWFzZTtcclxuICAgIC1tb3otdHJhbnNpdGlvbjogd2lkdGggMC4zcyBlYXNlO1xyXG4gICAgLW8tdHJhbnNpdGlvbjogd2lkdGggMC4zcyBlYXNlO1xyXG4gICAgdHJhbnNpdGlvbjogd2lkdGggMC4zcyBlYXNlO1xyXG59XHJcblxyXG4uaGlkZXBhZGRpbmcge1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1MHB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogNTBweDtcclxufVxyXG5cclxuLnNob3dwYWRkaW5nIHtcclxuICAgIHBhZGRpbmctbGVmdDogMHB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogMHB4O1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gICAgd2lkdGg6IDExMnB4O1xyXG4gICAgaGVpZ2h0OiAzM3B4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLmFjY29yZGlvbi1jb2wge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLXByaW1hcnktYmFja2dyb3VuZCk7XHJcbiAgICBtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gMjMycHgpO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICAuYWNjb3JkaW9uIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlYmY2O1xyXG4gICAgICAgIGNvbG9yOiAjNDQ0O1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAxMnB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogMC42cztcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB9XHJcbiAgICAuYWN0aXZlLFxyXG4gICAgLmFjY29yZGlvbjpob3ZlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2VlZWJmNjtcclxuICAgIH1cclxuICAgIC5hY2NvcmRpb246YWZ0ZXIge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDogMjBweDtcclxuICAgICAgICB0b3A6IDVweDtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1MCUsNTAlKTtcclxuICAgICAgICBjb250ZW50OiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvaW1hZ2VzL0ljb25cXCBhd2Vzb21lLWFuZ2xlLWRvd24ucG5nXCIpO1xyXG4gICAgfVxyXG4gICAgLmFjdGl2ZTphZnRlciB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHJpZ2h0OiAyMHB4O1xyXG4gICAgICAgIHRvcDogNXB4O1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDUwJSw1MCUpO1xyXG4gICAgICAgIGNvbnRlbnQ6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvSWNvblxcIGF3ZXNvbWUtYW5nbGUtdXAucG5nXCIpO1xyXG4gICAgfVxyXG4gICAgLnBhbmVsIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlYmY2O1xyXG4gICAgICAgIG1heC1oZWlnaHQ6IDA7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICB0cmFuc2l0aW9uOiBtYXgtaGVpZ2h0IDAuNHMgZWFzZS1vdXQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ 4385:
/*!**************************************************************************!*\
  !*** ./src/app/pages/first/session-screen/session-screen.component.scss ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".container {\n  white-space: nowrap;\n  display: flex;\n}\n\n.column50 {\n  width: 100%;\n}\n\n.column100 {\n  width: 100%;\n}\n\n.secondDiv {\n  width: 68px;\n  text-align: center;\n  padding-top: 20px;\n  background-color: var(--ion-primary-background);\n  border-top-left-radius: 30px;\n  border-bottom-left-radius: 30px;\n  margin-bottom: 65px;\n  transition: width 0.3s ease;\n  min-height: calc(100vh - 232px);\n}\n\n.secondDivWidth {\n  width: 300px;\n  background-color: var(--ion-primary-background);\n  border-top-left-radius: 30px;\n  border-bottom-left-radius: 30px;\n  margin-bottom: 65px;\n  transition: width 0.3s ease;\n  min-height: calc(100vh - 232px);\n}\n\n.page-header {\n  text-align: center;\n  background-color: var(--ion-primary-background);\n}\n\n.page-header h2 {\n  padding-top: 5px;\n  padding-bottom: 5px;\n  color: var(--ion-color-primary);\n  margin: 0px !important;\n}\n\nion-grid {\n  padding: 0px !important;\n}\n\n.page-title {\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 1px solid var(--ion-color-primary);\n  padding-top: 20px;\n  padding-bottom: 20px;\n  padding-left: 45px;\n  padding-right: 45px;\n}\n\n.page-title .page-topic-div {\n  padding-top: 5px;\n  padding-bottom: 5px;\n}\n\n.page-title .page-topic-div .page-topic-title {\n  display: inline;\n  color: #707070;\n}\n\n.page-title .page-topic-div .page-topic-content {\n  display: inline;\n  color: var(--ion-color-secondary);\n}\n\n.page-title .phase-btn {\n  background: var(--ion-color-primary);\n  padding: 5px;\n  color: #ffffff;\n  border-radius: 10px;\n}\n\n.table-col {\n  padding: 20px;\n}\n\n.table-col .record-data {\n  text-align: center;\n  margin-top: 50px;\n}\n\n.table-col .record-data .record-btn {\n  font-size: 22px !important;\n  padding: 20px 30px;\n  border-radius: 2px;\n}\n\n.table-col .record-box {\n  display: flex;\n  justify-content: center;\n}\n\n.table-col .record-box .record-box-div {\n  margin-top: 50px;\n  border: 1px solid var(--ion-color-primary);\n  width: 70%;\n  border-radius: 20px;\n}\n\n.table-col .record-box .record-box-div .record-box-div-header {\n  text-align: center;\n}\n\n.table-col .record-box .record-box-div .record-box-div-header h6 {\n  margin: 0px !important;\n  background: var(--ion-color-primary);\n  border-top-left-radius: 20px;\n  border-top-right-radius: 20px;\n  color: var(--white-color);\n  padding-top: 5px;\n  padding-bottom: 5px;\n}\n\n.table-col .record-box ul {\n  padding-left: 25px !important;\n  padding-right: 25px !important;\n  padding-bottom: 5px;\n}\n\n.table-col .record-box ul li {\n  list-style: none;\n}\n\n.table-col .table-grid {\n  margin-left: 70px;\n  margin-right: 70px;\n  margin-top: 50px;\n  padding: 0px;\n  border: 1px solid var(--ion-color-primary);\n}\n\n.table-col .table-grid .table-row {\n  background-color: #dce0e8;\n}\n\n.table-col .table-grid .odd {\n  background: #ffffff;\n}\n\n.table-col .table-grid .odd ion-col {\n  border-right: 1px solid var(--ion-color-primary);\n  text-align: center;\n}\n\n.table-col .table-grid .even {\n  background: #faf6fd;\n}\n\n.table-col .table-grid .even ion-col {\n  border-right: 1px solid var(--ion-color-primary);\n  text-align: center;\n}\n\n.table-col .table-grid ion-col {\n  text-align: center;\n}\n\nion-col {\n  padding: 0px !important;\n}\n\nion-col .collapse-col {\n  background-color: var(--ion-primary-background);\n  min-height: 100vh;\n}\n\nion-col .collapse-col .collapse-div {\n  background-color: #eeebf6;\n  color: #444;\n  cursor: pointer;\n  padding: 12px;\n  width: 100%;\n  border: none;\n  text-align: left;\n  outline: none;\n  transition: 0.6s;\n  position: relative;\n}\n\nion-col .collapse-col .active,\nion-col .collapse-col .collapse-div:hover {\n  background-color: #eeebf6;\n}\n\nion-col .collapse-col .collapse-div:after {\n  position: absolute;\n  right: 20px;\n  top: 5px;\n  transform: translate(50%, 50%);\n  content: url('Icon awesome-angle-down.png');\n}\n\nion-col .collapse-col .active:after {\n  position: absolute;\n  right: 20px;\n  top: 5px;\n  transform: translate(50%, 50%);\n  content: url('Icon awesome-angle-up.png');\n}\n\nion-col .collapse-col .collapse-panel {\n  background-color: #eeebf6;\n  max-height: 0;\n  overflow: hidden;\n  white-space: pre-wrap;\n  transition: max-height 0.4s ease-out;\n}\n\n.page-footer {\n  text-align: center;\n  width: 100%;\n  background: var(--ion-color-secondary);\n  border-top-left-radius: 20px;\n  border-top-right-radius: 20px;\n  display: flex;\n  align-items: center;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n\n.page-footer .footer-menu {\n  padding: 12px 20px;\n  color: #ffffff;\n  display: flex;\n  align-items: center;\n}\n\n.page-footer .footer-menu span {\n  padding-left: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlc3Npb24tc2NyZWVuLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxhQUFBO0FBQ0o7O0FBQ0E7RUFDSSxXQUFBO0FBRUo7O0FBQ0E7RUFDSSxXQUFBO0FBRUo7O0FBQ0E7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLCtDQUFBO0VBQ0EsNEJBQUE7RUFDQSwrQkFBQTtFQUNBLG1CQUFBO0VBSUEsMkJBQUE7RUFDQSwrQkFBQTtBQUVKOztBQUNBO0VBQ0ksWUFBQTtFQUNBLCtDQUFBO0VBQ0EsNEJBQUE7RUFDQSwrQkFBQTtFQUNBLG1CQUFBO0VBSUEsMkJBQUE7RUFDQSwrQkFBQTtBQUVKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSwrQ0FBQTtBQUVKOztBQURJO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLCtCQUFBO0VBQ0Esc0JBQUE7QUFHUjs7QUFDQTtFQUNJLHVCQUFBO0FBRUo7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxpREFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBRUo7O0FBREk7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FBR1I7O0FBRlE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQUlaOztBQUZRO0VBQ0ksZUFBQTtFQUNBLGlDQUFBO0FBSVo7O0FBREk7RUFDSSxvQ0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFHUjs7QUFDQTtFQUNJLGFBQUE7QUFFSjs7QUFESTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7QUFHUjs7QUFGUTtFQUNJLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUlaOztBQURJO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBR1I7O0FBRlE7RUFDSSxnQkFBQTtFQUNBLDBDQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FBSVo7O0FBSFk7RUFDSSxrQkFBQTtBQUtoQjs7QUFKZ0I7RUFDSSxzQkFBQTtFQUNBLG9DQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQU1wQjs7QUFGUTtFQUNJLDZCQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQUlaOztBQUhZO0VBQ0ksZ0JBQUE7QUFLaEI7O0FBQUk7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsMENBQUE7QUFFUjs7QUFEUTtFQUNJLHlCQUFBO0FBR1o7O0FBRFE7RUFDSSxtQkFBQTtBQUdaOztBQUZZO0VBQ0ksZ0RBQUE7RUFDQSxrQkFBQTtBQUloQjs7QUFEUTtFQUNJLG1CQUFBO0FBR1o7O0FBRlk7RUFDSSxnREFBQTtFQUNBLGtCQUFBO0FBSWhCOztBQURRO0VBQ0ksa0JBQUE7QUFHWjs7QUFFQTtFQUNJLHVCQUFBO0FBQ0o7O0FBQUk7RUFDSSwrQ0FBQTtFQUNBLGlCQUFBO0FBRVI7O0FBRFE7RUFDSSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQUdaOztBQURROztFQUVJLHlCQUFBO0FBR1o7O0FBRFE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsOEJBQUE7RUFDQSwyQ0FBQTtBQUdaOztBQURRO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLDhCQUFBO0VBQ0EseUNBQUE7QUFHWjs7QUFEUTtFQUNJLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQ0FBQTtBQUdaOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0NBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtBQUNKOztBQUFJO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FBRVI7O0FBRFE7RUFDSSxrQkFBQTtBQUdaIiwiZmlsZSI6InNlc3Npb24tc2NyZWVuLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG4uY29sdW1uNTAge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5jb2x1bW4xMDAge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5zZWNvbmREaXYge1xyXG4gICAgd2lkdGg6IDY4cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1wcmltYXJ5LWJhY2tncm91bmQpO1xyXG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcclxuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDMwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA2NXB4O1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiB3aWR0aCAwLjNzIGVhc2U7XHJcbiAgICAtbW96LXRyYW5zaXRpb246IHdpZHRoIDAuM3MgZWFzZTtcclxuICAgIC1vLXRyYW5zaXRpb246IHdpZHRoIDAuM3MgZWFzZTtcclxuICAgIHRyYW5zaXRpb246IHdpZHRoIDAuM3MgZWFzZTtcclxuICAgIG1pbi1oZWlnaHQ6IGNhbGMoMTAwdmggLSAyMzJweCk7XHJcbn1cclxuXHJcbi5zZWNvbmREaXZXaWR0aCB7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tcHJpbWFyeS1iYWNrZ3JvdW5kKTtcclxuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XHJcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAzMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNjVweDtcclxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogd2lkdGggMC4zcyBlYXNlO1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiB3aWR0aCAwLjNzIGVhc2U7XHJcbiAgICAtby10cmFuc2l0aW9uOiB3aWR0aCAwLjNzIGVhc2U7XHJcbiAgICB0cmFuc2l0aW9uOiB3aWR0aCAwLjNzIGVhc2U7XHJcbiAgICBtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gMjMycHgpO1xyXG59XHJcblxyXG4ucGFnZS1oZWFkZXIge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLXByaW1hcnktYmFja2dyb3VuZCk7XHJcbiAgICBoMiB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAgICAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuaW9uLWdyaWQge1xyXG4gICAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wYWdlLXRpdGxlIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcclxuICAgIHBhZGRpbmctbGVmdDogNDVweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDQ1cHg7XHJcbiAgICAucGFnZS10b3BpYy1kaXYge1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgICAucGFnZS10b3BpYy10aXRsZSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgICAgICAgICAgY29sb3I6ICM3MDcwNzA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5wYWdlLXRvcGljLWNvbnRlbnQge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmU7XHJcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAucGhhc2UtYnRuIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50YWJsZS1jb2wge1xyXG4gICAgcGFkZGluZzogMjBweDtcclxuICAgIC5yZWNvcmQtZGF0YSB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgICAgICAgLnJlY29yZC1idG4ge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIycHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgcGFkZGluZzogMjBweCAzMHB4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnJlY29yZC1ib3gge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgLnJlY29yZC1ib3gtZGl2IHtcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgICAgICAgICB3aWR0aDogNzAlO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgICAgICAucmVjb3JkLWJveC1kaXYtaGVhZGVyIHtcclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGg2IHtcclxuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgICAgICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB2YXIoLS13aGl0ZS1jb2xvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHVsIHtcclxuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAyNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDI1cHggIWltcG9ydGFudDtcclxuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgICAgICAgbGkge1xyXG4gICAgICAgICAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgICAgICAgICAgICAgIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBibGFjaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC50YWJsZS1ncmlkIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNzBweDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDcwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgICAgIC50YWJsZS1yb3cge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGNlMGU4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAub2RkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgICAgICAgICAgaW9uLWNvbCB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmV2ZW4ge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmFmNmZkO1xyXG4gICAgICAgICAgICBpb24tY29sIHtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpb24tY29sIHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuaW9uLWNvbCB7XHJcbiAgICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcclxuICAgIC5jb2xsYXBzZS1jb2wge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1wcmltYXJ5LWJhY2tncm91bmQpO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xyXG4gICAgICAgIC5jb2xsYXBzZS1kaXYge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlYmY2O1xyXG4gICAgICAgICAgICBjb2xvcjogIzQ0NDtcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMnB4O1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjZzO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5hY3RpdmUsXHJcbiAgICAgICAgLmNvbGxhcHNlLWRpdjpob3ZlciB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWViZjY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb2xsYXBzZS1kaXY6YWZ0ZXIge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHJpZ2h0OiAyMHB4O1xyXG4gICAgICAgICAgICB0b3A6IDVweDtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoNTAlLCA1MCUpO1xyXG4gICAgICAgICAgICBjb250ZW50OiB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL0ljb25cXCBhd2Vzb21lLWFuZ2xlLWRvd24ucG5nXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYWN0aXZlOmFmdGVyIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICByaWdodDogMjBweDtcclxuICAgICAgICAgICAgdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDUwJSwgNTAlKTtcclxuICAgICAgICAgICAgY29udGVudDogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9JY29uXFwgYXdlc29tZS1hbmdsZS11cC5wbmdcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb2xsYXBzZS1wYW5lbCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlZWViZjY7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDA7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogbWF4LWhlaWdodCAwLjRzIGVhc2Utb3V0O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnBhZ2UtZm9vdGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyMHB4O1xyXG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDIwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgLmZvb3Rlci1tZW51IHtcclxuICAgICAgICBwYWRkaW5nOiAxMnB4IDIwcHg7XHJcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5cclxuIl19 */");

/***/ }),

/***/ 6954:
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.page.scss ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".active-item {\n  background-image: linear-gradient(to right, transparent, #EB2168);\n}\n\n.split-pane-visible > ion-menu {\n  min-width: 96px;\n  max-width: 96px;\n  --ion-background-color: var(--ion-color-primary);\n}\n\n.app-icon {\n  height: 96px;\n  max-width: 96px !important;\n}\n\n.menu-list {\n  --border-style: none !important;\n  padding-top: 5px;\n  padding-bottom: 5px;\n  margin-top: 20px;\n  margin-bottom: 20px;\n}\n\nion-list {\n  margin-top: 100px;\n}\n\n.logout-icon {\n  position: fixed;\n  bottom: 0;\n  text-align: center;\n  width: 100%;\n  padding-bottom: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1lbnUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksaUVBQUE7QUFBSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0EsZ0RBQUE7QUFBSjs7QUFHQTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtBQUFKOztBQUdBO0VBQ0ksK0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7QUFBSjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7QUFBSiIsImZpbGUiOiJtZW51LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hY3RpdmUtaXRlbSB7XHJcbiAgICAvLyBib3JkZXItbGVmdDogOHB4IHNvbGlkICNmZmZmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsIHRyYW5zcGFyZW50ICwgI0VCMjE2OCk7XHJcbn1cclxuXHJcbi5zcGxpdC1wYW5lLXZpc2libGUgPiBpb24tbWVudSB7XHJcbiAgICBtaW4td2lkdGg6IDk2cHg7XHJcbiAgICBtYXgtd2lkdGg6IDk2cHg7XHJcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbn1cclxuXHJcbi5hcHAtaWNvbiB7XHJcbiAgICBoZWlnaHQ6IDk2cHg7XHJcbiAgICBtYXgtd2lkdGg6IDk2cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLm1lbnUtbGlzdCB7XHJcbiAgICAtLWJvcmRlci1zdHlsZTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuaW9uLWxpc3Qge1xyXG4gICAgbWFyZ2luLXRvcDogMTAwcHg7XHJcbn1cclxuXHJcbi5sb2dvdXQtaWNvbiB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyNXB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 7199:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/first/first.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\r\n  <div\r\n    style=\"display: flex; justify-content: space-between; padding: 30px; border-bottom: 1px solid var(--ion-color-primary);\">\r\n    <div style=\"display: flex; flex-direction: row; width: 50%;\">\r\n      <img src=\"../../../assets/icon/avatar.svg\" />\r\n      <div style=\"margin-left: 25px;\">\r\n        <h2>Welcome, John</h2>\r\n        <p>Today is a great day</p>\r\n      </div>\r\n    </div>\r\n    <div style=\"width: 50%;\">\r\n      <!-- <ion-toolbar> -->\r\n      <ion-searchbar></ion-searchbar>\r\n      <!-- </ion-toolbar> -->\r\n    </div>\r\n  </div>\r\n\r\n  <div style=\"padding-top: 40px; padding-bottom: 40px; text-align: center;\">\r\n    <h1 style=\"color: var(--ion-color-primary);\">Today's Sessions</h1>\r\n  </div>\r\n  <div class=\"container-div\">\r\n    <div [ngClass]=\"isShown ? 'div1Class': 'div1Class2'\">\r\n\r\n      <div [ngClass]=\"isShown ? 'hidepadding': 'showpadding'\">\r\n        <ion-grid class=\"table-grid\">\r\n          <ion-row class=\"table-row\" style=\" background-color: #faf6fd;\">\r\n            <ion-col size=\"4\" style=\"text-align: center;\">\r\n              <p style=\"font-weight: bold; color: var(--ion-color-primary);\">Learner</p>\r\n            </ion-col>\r\n            <ion-col size=\"3\" style=\"text-align: center;\">\r\n              <p style=\"font-weight: bold; color: var(--ion-color-primary);\">Session Profile</p>\r\n            </ion-col>\r\n            <ion-col size=\"5\" style=\"text-align: center;\">\r\n              <p style=\"font-weight: bold; color: var(--ion-color-primary);\">Select Action</p>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row class=\"table-content-col\"\r\n            style=\"border-bottom: 1px solid grey; padding-top: 20px;padding-bottom: 20px;\">\r\n            <ion-col size=\"4\" style=\"display: flex; justify-content: center; align-items: center;\">\r\n              <p>Emilia Stephanie</p>\r\n            </ion-col>\r\n            <ion-col size=\"3\" style=\"display: flex; justify-content: center; align-items: center;\">\r\n              <h4 style=\"font-weight: bold; color: var(--ion-color-primary);\">Session 01</h4>\r\n            </ion-col>\r\n            <ion-col size=\"5\" style=\"text-align: center;\">\r\n              <div style=\"display: flex; flex-direction: row; justify-content: center; align-items: center;\">\r\n                <button (click)=\"startSession()\">Start Session</button>\r\n                <button (click)=\"showSideDrawer()\">View Session</button>\r\n              </div>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </div>\r\n    </div>\r\n    <div [ngClass]=\"isShown ? 'div2Class': 'div2Class2'\">\r\n      <div style=\"padding-top: 33px; border-bottom: 1px solid grey;\">\r\n      </div>\r\n      <div style=\"text-align: center;\">\r\n        <h5 style=\"padding-top: 20px;padding-bottom: 20px;\">List of Clinical Programs</h5>\r\n        <div class=\"accordion-col\">\r\n          <div class=\"accordion\">\r\n            <h6 style=\"margin: 0px;\">Matching</h6>\r\n          </div>\r\n          <div class=\"panel\">\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore\r\n            </p>\r\n          </div>\r\n          <div class=\"accordion\">\r\n            <h6 style=\"margin: 0px;\">Matching</h6>\r\n          </div>\r\n          <div class=\"panel\">\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore\r\n            </p>\r\n          </div>\r\n          <div class=\"accordion\">\r\n            <h6 style=\"margin: 0px;\">Matching</h6>\r\n          </div>\r\n          <div class=\"panel\">\r\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore\r\n            </p>\r\n          </div>\r\n          <div style=\"margin-top: 50px;\">\r\n            <button (click)=\"startSession()\">Start Session</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n\r\n\r\n  <!-- <ion-toolbar>\r\n    <ion-list>\r\n      <ion-item *ngFor=\"let post of posts\">\r\n        <ion-label>{{ post.id }}: {{ post.title }}</ion-label>\r\n      </ion-item>\r\n    </ion-list>\r\n  </ion-toolbar> -->\r\n</ion-content>");

/***/ }),

/***/ 6690:
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/first/session-screen/session-screen.component.html ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content [fullscreen]=\"true\">\r\n  <div class=\"page-header\">\r\n    <h2>Bresh Teeth Training</h2>\r\n  </div>\r\n  <div class=\"page-title\">\r\n    <div>\r\n      <div class=\"page-topic-div\">\r\n        <p class=\"page-topic-title\">NAME OF PROGRAM - </p>\r\n        <p class=\"page-topic-content\">Brush teeth training</p>\r\n      </div>\r\n      <div class=\"page-topic-div\">\r\n        <p class=\"page-topic-title\">TARGET - </p>\r\n        <p class=\"page-topic-content\">Brush</p>\r\n      </div>\r\n      <div class=\"page-topic-div\">\r\n        <p class=\"page-topic-title\">PHASE - </p>\r\n        <p class=\"page-topic-content\">Regress</p>\r\n      </div>\r\n    </div>\r\n    <div>\r\n      <div class=\"phase-btn\">\r\n        <span>Regress Phase</span>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"container\">\r\n    <div [ngClass]=\" showId ? 'column100' : 'column50'\">\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col size=\"3\">\r\n            <div class=\"collapse-col\">\r\n              <div class=\"collapse-div\">\r\n                <h6 style=\"margin: 0px;\">Matching</h6>\r\n              </div>\r\n              <div class=\"collapse-panel\">\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"9\" class=\"table-col\">\r\n            <ion-grid class=\"table-grid\">\r\n              <ion-row class=\"table-row\">\r\n                <ion-col size=\"4\">Steps</ion-col>\r\n                <ion-col size=\"1\">0S</ion-col>\r\n                <ion-col size=\"1\">2S</ion-col>\r\n                <ion-col size=\"1\">4S</ion-col>\r\n                <ion-col size=\"3\">Correct</ion-col>\r\n                <ion-col size=\"2\">Incorrect</ion-col>\r\n              </ion-row>\r\n              <ion-row [ngClass]=\"(0 % 2 == 0) ? 'odd' : 'even'\">\r\n                <ion-col size=\"4\">1</ion-col>\r\n                <ion-col size=\"1\">2</ion-col>\r\n                <ion-col size=\"1\">3</ion-col>\r\n                <ion-col size=\"1\">4</ion-col>\r\n                <ion-col size=\"3\">5</ion-col>\r\n                <ion-col size=\"2\">6</ion-col>\r\n              </ion-row>\r\n              <ion-row [ngClass]=\"(1 % 2 == 0) ? 'odd' : 'even'\">\r\n                <ion-col size=\"4\">1</ion-col>\r\n                <ion-col size=\"1\">2</ion-col>\r\n                <ion-col size=\"1\">3</ion-col>\r\n                <ion-col size=\"1\">4</ion-col>\r\n                <ion-col size=\"3\">5</ion-col>\r\n                <ion-col size=\"2\">6</ion-col>\r\n              </ion-row>\r\n              <ion-row [ngClass]=\"(2 % 2 == 0) ? 'odd' : 'even'\">\r\n                <ion-col size=\"4\">1</ion-col>\r\n                <ion-col size=\"1\">2</ion-col>\r\n                <ion-col size=\"1\">3</ion-col>\r\n                <ion-col size=\"1\">4</ion-col>\r\n                <ion-col size=\"3\">5</ion-col>\r\n                <ion-col size=\"2\">6</ion-col>\r\n              </ion-row>\r\n            </ion-grid>\r\n            <div class=\"record-data\">\r\n              <button class=\"record-btn secondary-button\">Record Data</button>\r\n            </div>\r\n            <div class=\"record-box\">\r\n              <div class=\"record-box-div\">\r\n                <div class=\"record-box-div-header\">\r\n                  <h6>\r\n                    Previous Records</h6>\r\n                </div>\r\n                <div>\r\n                  <ul>\r\n                    <li>\r\n                      1. sasas\r\n                    </li>\r\n                    <li>\r\n                      2. sasas\r\n                    </li>\r\n                    <li>\r\n                      2. sasas\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </div>\r\n\r\n    <div [ngClass]=\" showId ? 'secondDiv' : 'secondDivWidth'\">\r\n      <img (click)=\"toggleId()\" src=\"../../../assets/icon/application.svg\" />\r\n      <p *ngIf=\"!showId\">content inside</p>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n<ion-footer>\r\n  <div class=\"page-footer\">\r\n    <div class=\"footer-menu\">\r\n      <img src=\"../../../assets/images/timer.png\" />\r\n      <span>00:00:00</span>\r\n    </div>\r\n    <div class=\"footer-menu\">\r\n      <img src=\"../../../assets/images/notes.png\" />\r\n      <span>ADD NOTES</span>\r\n    </div>\r\n    <div class=\"footer-menu\">\r\n      <img src=\"../../../assets/images/summary.png\" />\r\n      <span>SUMMARY SESSION NOTE</span>\r\n    </div>\r\n    <div class=\"footer-menu\">\r\n      <img src=\"../../../assets/images/help.png\" />\r\n      <span>HELP?</span>\r\n    </div>\r\n  </div>\r\n</ion-footer>");

/***/ }),

/***/ 538:
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/menu.page.html ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-split-pane when=\"(min-width: 40px)\" contentId=\"content\" class=\"my-custom-menu\">\n  <!--  the side menu  -->\n  <ion-menu contentId=\"content\">\n    <!-- <ion-header>\n      <ion-toolbar>\n        <ion-title>Menu</ion-title>\n      </ion-toolbar>\n    </ion-header> -->\n\n    <ion-content class=\"ion-text-center\">\n\n      <img (click)=\"gotoHome()\" class=\"app-icon\" src=\"../../../assets/images/app-icon.png\" />\n\n      <ion-list>\n        <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of pages\">\n          <div class=\"menu-list\" [routerLink]=\"p.url\" routerDirection=\"root\"\n            [class.active-item]=\"selectedPath === p.url\">\n            <!-- {{p.title}} -->\n            <img [src]=\"p.title\" />\n          </div>\n        </ion-menu-toggle>\n      </ion-list>\n\n      <div (click)=\"logOut()\" class=\"logout-icon\">\n        <img src=\"../../../assets/images/Logout.png\" />\n      </div>\n\n    </ion-content>\n  </ion-menu>\n\n  <!-- the content content -->\n  <ion-router-outlet id=\"content\" main></ion-router-outlet>\n</ion-split-pane>\n\n<!-- <ion-split-pane>\n  <ion-menu contentId=\"content\">\n    <ion-header>\n      <ion-toolbar>\n        <ion-title>\n          Menu\n        </ion-title>\n      </ion-toolbar>\n    </ion-header>\n    <ion-content>\n      <ion-list>\n        <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of pages\">\n          <ion-item [routerLink]=\"p.url\" routerDirection=\"root\" [class.active-item]=\"selectedPath === p.url\">\n            {{p.title}}\n          </ion-item>\n        </ion-menu-toggle>\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n  <ion-router-outlet id='content' main></ion-router-outlet>\n</ion-split-pane> -->");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_menu_menu_module_ts.js.map