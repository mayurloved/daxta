(self["webpackChunkDaxta"] = self["webpackChunkDaxta"] || []).push([["src_app_pages_third_third_module_ts"],{

/***/ 7046:
/*!*****************************************************!*\
  !*** ./src/app/pages/third/third-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ThirdPageRoutingModule": () => (/* binding */ ThirdPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _third_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./third.page */ 181);




const routes = [
    {
        path: '',
        component: _third_page__WEBPACK_IMPORTED_MODULE_0__.ThirdPage
    }
];
let ThirdPageRoutingModule = class ThirdPageRoutingModule {
};
ThirdPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ThirdPageRoutingModule);



/***/ }),

/***/ 4812:
/*!*********************************************!*\
  !*** ./src/app/pages/third/third.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ThirdPageModule": () => (/* binding */ ThirdPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _third_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./third-routing.module */ 7046);
/* harmony import */ var _third_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./third.page */ 181);







let ThirdPageModule = class ThirdPageModule {
};
ThirdPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _third_routing_module__WEBPACK_IMPORTED_MODULE_0__.ThirdPageRoutingModule
        ],
        declarations: [_third_page__WEBPACK_IMPORTED_MODULE_1__.ThirdPage]
    })
], ThirdPageModule);



/***/ }),

/***/ 181:
/*!*******************************************!*\
  !*** ./src/app/pages/third/third.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ThirdPage": () => (/* binding */ ThirdPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_third_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./third.page.html */ 1522);
/* harmony import */ var _third_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./third.page.scss */ 900);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/authentication/authentication.service */ 7020);
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/storage/storage.service */ 6578);







let ThirdPage = class ThirdPage {
    constructor(platform, authentication, alrtCtrl, storageservice, navCtrl) {
        this.platform = platform;
        this.authentication = authentication;
        this.alrtCtrl = alrtCtrl;
        this.storageservice = storageservice;
        this.navCtrl = navCtrl;
        this.showId = true;
    }
    ngOnInit() {
    }
};
ThirdPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.Platform },
    { type: src_app_services_authentication_authentication_service__WEBPACK_IMPORTED_MODULE_2__.AuthenticationService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.AlertController },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.NavController }
];
ThirdPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-third',
        template: _raw_loader_third_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_third_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ThirdPage);



/***/ }),

/***/ 900:
/*!*********************************************!*\
  !*** ./src/app/pages/third/third.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0aGlyZC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 1522:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/third/third.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content [fullscreen]=\"true\">\n <div>\n   <p>Change Password</p>\n </div>\n</ion-content>\n<!-- <ion-footer>\n  <div class=\"page-footer\">\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/timer.png\" />\n      <span>00:00:00</span>\n    </div>\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/notes.png\" />\n      <span>ADD NOTES</span>\n    </div>\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/summary.png\" />\n      <span>SUMMARY SESSION NOTE</span>\n    </div>\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/help.png\" />\n      <span>HELP?</span>\n    </div>\n  </div>\n</ion-footer> -->");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_third_third_module_ts.js.map