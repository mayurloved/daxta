(self["webpackChunkDaxta"] = self["webpackChunkDaxta"] || []).push([["src_app_pages_second_second_module_ts"],{

/***/ 5729:
/*!*******************************************************!*\
  !*** ./src/app/pages/second/second-routing.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SecondPageRoutingModule": () => (/* binding */ SecondPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _second_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./second.page */ 5137);




const routes = [
    {
        path: '',
        component: _second_page__WEBPACK_IMPORTED_MODULE_0__.SecondPage
    }
];
let SecondPageRoutingModule = class SecondPageRoutingModule {
};
SecondPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SecondPageRoutingModule);



/***/ }),

/***/ 4702:
/*!***********************************************!*\
  !*** ./src/app/pages/second/second.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SecondPageModule": () => (/* binding */ SecondPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _second_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./second-routing.module */ 5729);
/* harmony import */ var _second_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./second.page */ 5137);







let SecondPageModule = class SecondPageModule {
};
SecondPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _second_routing_module__WEBPACK_IMPORTED_MODULE_0__.SecondPageRoutingModule
        ],
        declarations: [_second_page__WEBPACK_IMPORTED_MODULE_1__.SecondPage]
    })
], SecondPageModule);



/***/ }),

/***/ 5137:
/*!*********************************************!*\
  !*** ./src/app/pages/second/second.page.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SecondPage": () => (/* binding */ SecondPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_second_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./second.page.html */ 5283);
/* harmony import */ var _second_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./second.page.scss */ 2081);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/core */ 8384);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/storage/storage.service */ 6578);
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! apollo-angular */ 550);
/* harmony import */ var src_app_graphqlQURIES__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/graphqlQURIES */ 7436);








const { App } = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__.Plugins;



let SecondPage = class SecondPage {
    constructor(platform, routerOutlet, navCtrl, router, storageservice, apollo, alertController) {
        this.platform = platform;
        this.routerOutlet = routerOutlet;
        this.navCtrl = navCtrl;
        this.router = router;
        this.storageservice = storageservice;
        this.apollo = apollo;
        this.alertController = alertController;
        this.posts = [];
    }
    ngOnInit() {
        this.getPostofAuthor();
        this.accordionToggle();
    }
    accordionToggle() {
        var acc = document.getElementsByClassName('accordion');
        var i;
        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener('click', function () {
                this.classList.toggle('active');
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                }
                else {
                    panel.style.maxHeight = panel.scrollHeight + 'px';
                }
            });
        }
    }
    getPostofAuthor() {
        this.apollo
            .watchQuery({
            query: src_app_graphqlQURIES__WEBPACK_IMPORTED_MODULE_4__.GET_POSTS_OF_AUTHOR,
            variables: {
                id: 1,
            },
        })
            .valueChanges.subscribe((result) => {
            var _a, _b, _c;
            this.posts = (_c = (_b = (_a = result === null || result === void 0 ? void 0 : result.data) === null || _a === void 0 ? void 0 : _a.user) === null || _b === void 0 ? void 0 : _b.posts) === null || _c === void 0 ? void 0 : _c.data;
            this.storageservice.set('myValue', this.posts);
        });
    }
    showAlert() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'The information you have provided is incomplete or invalid. Please check your entries and check again.'
            });
            alert.present();
            setTimeout(() => {
                alert.dismiss();
            }, 2000);
        });
    }
};
SecondPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonRouterOutlet },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: src_app_services_storage_storage_service__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: apollo_angular__WEBPACK_IMPORTED_MODULE_8__.Apollo },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.AlertController }
];
SecondPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-second',
        template: _raw_loader_second_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_second_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SecondPage);



/***/ }),

/***/ 2081:
/*!***********************************************!*\
  !*** ./src/app/pages/second/second.page.scss ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("@charset \"UTF-8\";\n.page-header {\n  text-align: center;\n  background-color: var(--ion-primary-background);\n}\n.page-header h2 {\n  padding-top: 5px;\n  padding-bottom: 5px;\n  color: var(--ion-color-primary);\n  margin: 0px !important;\n}\nion-grid {\n  padding: 0px !important;\n}\n.page-title {\n  display: flex;\n  justify-content: space-between;\n  border-bottom: 1px solid var(--ion-color-primary);\n  padding-top: 20px;\n  padding-bottom: 20px;\n  padding-left: 45px;\n  padding-right: 45px;\n}\n.page-title .page-topic-div {\n  padding-top: 5px;\n  padding-bottom: 5px;\n}\n.page-title .page-topic-div .page-topic-title {\n  display: inline;\n  color: #707070;\n}\n.page-title .page-topic-div .page-topic-content {\n  display: inline;\n  color: var(--ion-color-secondary);\n}\n.page-title .phase-btn {\n  background: var(--ion-color-primary);\n  padding: 5px;\n  color: #ffffff;\n  border-radius: 10px;\n}\n.table-col {\n  padding: 20px;\n}\n.table-col .table-grid {\n  padding: 0px;\n  border: 1px solid var(--ion-color-primary);\n}\n.table-col .table-grid .table-header-content {\n  background-color: var(--ion-color-primary);\n  color: #ffffff;\n}\n.table-col .table-grid .table-row {\n  background-color: #dce0e8;\n}\n.table-col .table-grid .table-row p {\n  font-weight: bold;\n}\n.table-col .table-grid .table-content-col ion-col {\n  border-right: 1px solid var(--ion-color-primary);\n}\n.table-col .table-grid .table-content-col ion-col:last-child {\n  border: none !important;\n}\n.table-col .table-grid ion-col {\n  text-align: center;\n}\n.accordion-col {\n  background-color: var(--ion-primary-background);\n  min-height: calc(100vh - 232px);\n}\n.accordion-col .accordion {\n  background-color: #eee;\n  color: #444;\n  cursor: pointer;\n  padding: 18px;\n  width: 100%;\n  border: none;\n  text-align: left;\n  outline: none;\n  transition: 0.6s;\n}\n.accordion-col .active,\n.accordion-col .accordion:hover {\n  background-color: #ccc;\n}\n.accordion-col .accordion:after {\n  content: \"+\";\n  color: #777;\n  font-weight: bold;\n  float: right;\n  margin-left: 5px;\n}\n.accordion-col .active:after {\n  content: \"−\";\n}\n.accordion-col .panel {\n  background-color: white;\n  max-height: 0;\n  overflow: hidden;\n  transition: max-height 0.4s ease-out;\n}\n.page-footer {\n  text-align: center;\n  width: 100%;\n  background: var(--ion-color-secondary);\n  border-top-left-radius: 20px;\n  border-top-right-radius: 20px;\n  display: flex;\n  align-items: center;\n  flex-direction: row;\n  justify-content: flex-end;\n}\n.page-footer .footer-menu {\n  padding: 12px 20px;\n  color: #ffffff;\n  display: flex;\n  align-items: center;\n}\n.page-footer .footer-menu span {\n  padding-left: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlY29uZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FBQWhCO0VBQ0ksa0JBQUE7RUFDQSwrQ0FBQTtBQUVKO0FBREk7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsK0JBQUE7RUFDQSxzQkFBQTtBQUdSO0FBQ0E7RUFDSSx1QkFBQTtBQUVKO0FBQ0E7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxpREFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBRUo7QUFESTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUFHUjtBQUZRO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUFJWjtBQUZRO0VBQ0ksZUFBQTtFQUNBLGlDQUFBO0FBSVo7QUFESTtFQUNJLG9DQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQUdSO0FBQ0E7RUFDSSxhQUFBO0FBRUo7QUFESTtFQUNJLFlBQUE7RUFDQSwwQ0FBQTtBQUdSO0FBRlE7RUFDSSwwQ0FBQTtFQUNBLGNBQUE7QUFJWjtBQUZRO0VBQ0kseUJBQUE7QUFJWjtBQUhZO0VBQ0ksaUJBQUE7QUFLaEI7QUFEWTtFQUNJLGdEQUFBO0FBR2hCO0FBRFk7RUFDSSx1QkFBQTtBQUdoQjtBQUNRO0VBQ0ksa0JBQUE7QUFDWjtBQUlBO0VBQ0ksK0NBQUE7RUFDQSwrQkFBQTtBQURKO0FBRUk7RUFDSSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFBUjtBQUVJOztFQUVJLHNCQUFBO0FBQVI7QUFFSTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFBUjtBQUVJO0VBQ0ksWUFBQTtBQUFSO0FBRUk7RUFDSSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLG9DQUFBO0FBQVI7QUFJQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHNDQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUFESjtBQUVJO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FBQVI7QUFDUTtFQUNJLGtCQUFBO0FBQ1oiLCJmaWxlIjoic2Vjb25kLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5wYWdlLWhlYWRlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLXByaW1hcnktYmFja2dyb3VuZCk7XG59XG4ucGFnZS1oZWFkZXIgaDIge1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xufVxuXG5pb24tZ3JpZCB7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG4ucGFnZS10aXRsZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDQ1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDQ1cHg7XG59XG4ucGFnZS10aXRsZSAucGFnZS10b3BpYy1kaXYge1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLnBhZ2UtdGl0bGUgLnBhZ2UtdG9waWMtZGl2IC5wYWdlLXRvcGljLXRpdGxlIHtcbiAgZGlzcGxheTogaW5saW5lO1xuICBjb2xvcjogIzcwNzA3MDtcbn1cbi5wYWdlLXRpdGxlIC5wYWdlLXRvcGljLWRpdiAucGFnZS10b3BpYy1jb250ZW50IHtcbiAgZGlzcGxheTogaW5saW5lO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG59XG4ucGFnZS10aXRsZSAucGhhc2UtYnRuIHtcbiAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4udGFibGUtY29sIHtcbiAgcGFkZGluZzogMjBweDtcbn1cbi50YWJsZS1jb2wgLnRhYmxlLWdyaWQge1xuICBwYWRkaW5nOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cbi50YWJsZS1jb2wgLnRhYmxlLWdyaWQgLnRhYmxlLWhlYWRlci1jb250ZW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbi50YWJsZS1jb2wgLnRhYmxlLWdyaWQgLnRhYmxlLXJvdyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkY2UwZTg7XG59XG4udGFibGUtY29sIC50YWJsZS1ncmlkIC50YWJsZS1yb3cgcCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnRhYmxlLWNvbCAudGFibGUtZ3JpZCAudGFibGUtY29udGVudC1jb2wgaW9uLWNvbCB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cbi50YWJsZS1jb2wgLnRhYmxlLWdyaWQgLnRhYmxlLWNvbnRlbnQtY29sIGlvbi1jb2w6bGFzdC1jaGlsZCB7XG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xufVxuLnRhYmxlLWNvbCAudGFibGUtZ3JpZCBpb24tY29sIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYWNjb3JkaW9uLWNvbCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1wcmltYXJ5LWJhY2tncm91bmQpO1xuICBtaW4taGVpZ2h0OiBjYWxjKDEwMHZoIC0gMjMycHgpO1xufVxuLmFjY29yZGlvbi1jb2wgLmFjY29yZGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XG4gIGNvbG9yOiAjNDQ0O1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHBhZGRpbmc6IDE4cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXI6IG5vbmU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG91dGxpbmU6IG5vbmU7XG4gIHRyYW5zaXRpb246IDAuNnM7XG59XG4uYWNjb3JkaW9uLWNvbCAuYWN0aXZlLFxuLmFjY29yZGlvbi1jb2wgLmFjY29yZGlvbjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjY2M7XG59XG4uYWNjb3JkaW9uLWNvbCAuYWNjb3JkaW9uOmFmdGVyIHtcbiAgY29udGVudDogXCIrXCI7XG4gIGNvbG9yOiAjNzc3O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuLmFjY29yZGlvbi1jb2wgLmFjdGl2ZTphZnRlciB7XG4gIGNvbnRlbnQ6IFwi4oiSXCI7XG59XG4uYWNjb3JkaW9uLWNvbCAucGFuZWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgbWF4LWhlaWdodDogMDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdHJhbnNpdGlvbjogbWF4LWhlaWdodCAwLjRzIGVhc2Utb3V0O1xufVxuXG4ucGFnZS1mb290ZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDIwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG4ucGFnZS1mb290ZXIgLmZvb3Rlci1tZW51IHtcbiAgcGFkZGluZzogMTJweCAyMHB4O1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5wYWdlLWZvb3RlciAuZm9vdGVyLW1lbnUgc3BhbiB7XG4gIHBhZGRpbmctbGVmdDogMTJweDtcbn0iXX0= */");

/***/ }),

/***/ 5283:
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/second/second.page.html ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div class=\"page-header\">\n    <h2>Bresh Teeth Training</h2>\n  </div>\n  <div class=\"page-title\">\n    <div>\n      <div class=\"page-topic-div\">\n        <p class=\"page-topic-title\">NAME OF PROGRAM - </p>\n        <p class=\"page-topic-content\">Brush teeth training</p>\n      </div>\n      <div class=\"page-topic-div\">\n        <p class=\"page-topic-title\">TARGET - </p>\n        <p class=\"page-topic-content\">Brush</p>\n      </div>\n      <div class=\"page-topic-div\">\n        <p class=\"page-topic-title\">PHASE - </p>\n        <p class=\"page-topic-content\">Regress</p>\n      </div>\n    </div>\n    <div>\n      <div class=\"phase-btn\">\n        <span>Regress Phase</span>\n      </div>\n    </div>\n  </div>\n\n\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"3\" class=\"accordion-col\">\n        <h2>Accordion</h2>\n        <div class=\"accordion\">\n          <button>Section 1</button>\n        </div>\n        <div class=\"panel\">\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>\n        </div>\n        <div class=\"accordion\">\n          <button>Section 2</button>\n        </div>\n        <div class=\"panel\">\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>\n        </div>\n        <div class=\"accordion\">\n          <button>Section 3</button>\n        </div>\n        <div class=\"panel\">\n          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>\n        </div>\n      </ion-col>\n      <ion-col size=\"9\" class=\"table-col\">\n        <ion-grid class=\"table-grid\">\n          <!-- <ion-row class=\"table-header-content\">\n            <ion-col>ion-col</ion-col>\n          </ion-row> -->\n          <ion-row class=\"table-row\">\n            <ion-col size=\"4\">\n              <p>Steps</p>\n            </ion-col>\n            <ion-col size=\"1\">\n              <p>0S</p>\n            </ion-col>\n            <ion-col size=\"1\">\n              <p>2S</p>\n            </ion-col>\n            <ion-col size=\"1\">\n              <p>\n                4S\n              </p>\n            </ion-col>\n            <ion-col size=\"3\">\n              <p>\n                Correct\n              </p>\n            </ion-col>\n            <ion-col size=\"2\">\n              <p>\n                Incorrect\n              </p>\n            </ion-col>\n          </ion-row>\n          <ion-row class=\"table-content-col\">\n            <ion-col size=\"4\">1</ion-col>\n            <ion-col size=\"1\">2</ion-col>\n            <ion-col size=\"1\">3</ion-col>\n            <ion-col size=\"1\">4</ion-col>\n            <ion-col size=\"3\">5</ion-col>\n            <ion-col size=\"2\">6</ion-col>\n          </ion-row>\n        </ion-grid>\n        <ion-button expand=\"full\" routerLink=\"/menu/first\">go to second menu</ion-button>\n        <ion-toolbar>\n          <ion-list>\n            <ion-item *ngFor=\"let post of posts\">\n              <ion-label>{{ post.id }}: {{ post.title }}</ion-label>\n            </ion-item>\n          </ion-list>\n        </ion-toolbar>\n        <ion-row>\n          <p (click)=\"showAlert()\">Show Alert</p>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n<ion-footer>\n  <div class=\"page-footer\">\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/timer.png\" />\n      <span>00:00:00</span>\n    </div>\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/notes.png\" />\n      <span>ADD NOTES</span>\n    </div>\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/summary.png\" />\n      <span>SUMMARY SESSION NOTE</span>\n    </div>\n    <div class=\"footer-menu\">\n      <img src=\"../../../assets/images/help.png\" />\n      <span>HELP?</span>\n    </div>\n  </div>\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_second_second_module_ts.js.map